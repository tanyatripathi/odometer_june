import valid_reading as vr


def next_reading(distance: int) -> int:
    next = distance + 1
    while not vr.valid_reading(next):
        if next < vr.ULIMIT:
            next += 1
        else:
            next = vr.LLIMIT
    return next


for i in vr.sys.argv[1:]:
    print(i, next_reading(int(i)))