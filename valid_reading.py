import sys

ODOMETER_DIGITS = len(sys.argv[1])
LLIMIT = int("123456789"[:ODOMETER_DIGITS])
ULIMIT = int("123456789"[-ODOMETER_DIGITS:])


def is_ascending(num: int) -> bool:
    return list(str(num)) == sorted(set(str(num)))


def valid_reading(distance: int) -> bool:
    return LLIMIT <= distance <= ULIMIT and is_ascending(distance)