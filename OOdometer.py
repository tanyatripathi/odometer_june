def is_ascending(n: int) -> bool:
    return all(a < b for a, b in zip(str(n), str(n)[1:]))


class Odometer:
    def __init__(self, size):
        self.size = size
        self.start = int("123456789"[:size])
        self.end = int("123456789"[-size:])
        self.reading = self.start

    def __str__(self):
        return str(self.reading)

    # def __repr__(self):     #represent - show others what this object is trying to do
    #    return f"{self.start} <<<< {self.reading} >>>> {self.end}"

    def forward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading == self.end:
                self.reading = self.start
            else:
                self.reading += 1
                while not is_ascending(self.reading):
                    self.reading += 1

    def backward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading == self.start:
                self.reading = self.end
            else:
                self.reading -= 1
                while not is_ascending(self.reading):
                    self.reading -= 1

    def steps(self, initial: int, final: int) -> int:
        self.reading = initial
        steps = 0
        while self.reading != final:
            steps += 1
            self.forward()
        return steps


Odometer1 = Odometer(3)

Odometer1.forward()
print(Odometer1)

Odometer1.backward()
print(Odometer1)

print(Odometer1.steps(1234, 1238))
