import sys

ODOMETER_DIGITS = len(sys.argv[1])
LLIMIT = int("123456789"[:ODOMETER_DIGITS])
ULIMIT = int("123456789"[-ODOMETER_DIGITS:])


def is_ascending(num: int) -> bool:
    return list(str(num)) == sorted(set(str(num)))


def valid_reading(distance: int) -> bool:
    return LLIMIT <= distance <= ULIMIT and is_ascending(distance)


def next_reading(distance: int) -> int:
    next = distance + 1
    while not valid_reading(next):
        if next < ULIMIT:
            next += 1
        else:
            next = LLIMIT
    return next


def find_reading(reading, n) -> int:
    while n > 0:
        reading = next_reading(reading)
        n-=1
    return reading


start = int(sys.argv[1])
n = int(sys.argv[2])
print(start, n, find_reading(start, n))