import sys

ODOMETER_DIGITS = len(sys.argv[1])
LLIMIT = int("123456789"[:ODOMETER_DIGITS])
ULIMIT = int("123456789"[-ODOMETER_DIGITS:])


def is_ascending(num: int) -> bool:
    return list(str(num)) == sorted(set(str(num)))


def valid_reading(distance: int) -> bool:
    return LLIMIT <= distance <= ULIMIT and is_ascending(distance)


def next_reading(distance: int) -> int:
    next = distance + 1
    while not valid_reading(next):
        if next < ULIMIT:
            next += 1
        else:
            next = LLIMIT
    return next


def find_distance(start, end) -> int:
    distance = 0
    while start != end:
        start = next_reading(start)
        distance += 1
    return distance


start = int(sys.argv[1])
end = int(sys.argv[2])
print(start, end, find_distance(start, end))
