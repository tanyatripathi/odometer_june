# Odometer Problem Statement

Consider an odometer with 4 digits. The digits must be in ascending order, and there are no 0's.

Write the following functions:

1. Given a reading, find the next reading.

2. Given a reading, find the previous reading.

3. Given two readings, find the distance between them.

4. Given a reading, find the reading $n$ steps after it.

