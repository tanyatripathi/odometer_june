import valid_reading as vr


def prev_reading(distance: int) -> int:
    prev = distance - 1
    while not vr.valid_reading(prev):
        if prev > vr.LLIMIT:
            prev -= 1
        else:
            prev = vr.ULIMIT
    return prev

for i in vr.sys.argv[1:]:
    print(i, prev_reading(int(i)))